import { Grid, Typography } from '@mui/material'
import React, { useContext } from 'react'
import "../styles/components/LengthBar.css"
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { Link } from 'react-router-dom';
import { StringifyOptions } from 'querystring';
import { CityInfoModel } from '../routes/Home';
import { AppInfoContext } from '../context/AppInfoContext';

function LengthBar() {
  const appInfoContext = useContext(AppInfoContext);

  const handleLengthChange = (newLength:string) => {
    appInfoContext.setTimeLength(newLength);
  }

  return (
    <Grid container xs={8} className="mainWrapperLength">
      <Grid container xs={11} className="contentWrapperLength">
        <Grid container xs={6} className="timeLengthWrapper">
          <Grid className="leftTextLength">
            <Link to="/" onClick={() => handleLengthChange("hourly")} style={{textDecoration: 'none'}}>
              <Typography className={appInfoContext.timeLength === "hourly" ? "selectedTextLen" : "notSelectedTextLen"}>HOURLY</Typography>
            </Link>
          </Grid>
          <Grid className="rightTextLength">
            <Link to="/7day" onClick={() => handleLengthChange("7Day")} style={{textDecoration: 'none'}}>
              <Typography className={appInfoContext.timeLength === "7Day" ? "selectedTextLen" : "notSelectedTextLen"}>7 DAYS</Typography>
            </Link>
          </Grid>
        </Grid>
        <Grid container xs={6} className="locationWrapper">
          <Link to="/edit-locations" onClick={() => handleLengthChange("none")} style={{textDecoration: 'none'}}>
            <Typography className='editLocationText'>VIEW/EDIT LOCATIONS</Typography>
          </Link>
          <Link to="/edit-locations" onClick={() => handleLengthChange("none")} style={{textDecoration: 'none'}}>
            <BorderColorIcon className='editLocationIcon' />
          </Link>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default LengthBar
import { Grid, Typography } from '@mui/material'
import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { CityInfoModel } from '../routes/Home';
import { AppInfoContext } from '../context/AppInfoContext';

function TitleSignInBar() {
  const appInfoContext = useContext(AppInfoContext);

  const handleLengthChange = (newLength:string) => {
    appInfoContext.setTimeLength(newLength);
  }

  return (
    <Grid>
      <Grid container xs={12} className="mainProfileWrapper">
        <Grid container className="mainContentWrapper">
          {
            appInfoContext.loggedIn === false ? 
            <Link to="/profile" style={{textDecoration: 'none'}} onClick={() => handleLengthChange("none")} >
              <Typography className="signInText">Sign in</Typography>
            </Link> : null
          }
          <Link to="/profile" onClick={() => handleLengthChange("none")}>
            <AccountCircleIcon className="profileIcon" />
          </Link>
        </Grid>
      </Grid>
      <Grid container xs={8} className="logoContainer">
        <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1646700395/WeatherApp/Group_3_1_t4jb8k.png" />
      </Grid>
    </Grid>
    
  )
}

export default TitleSignInBar
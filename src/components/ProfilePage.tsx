import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useContext, useState } from 'react'
import { AppInfoContext } from '../context/AppInfoContext';
import "../styles/components/Login.css";
import SettingsNavBar from './SettingsNavBar';

export type ProfilePageProps = {
  setHaveAccount: (account:any) => void
}

function ProfilePage({setHaveAccount}:ProfilePageProps) {
  const appInfoContext = useContext(AppInfoContext);
  const settingsSelection = "profile";

  const handleSignout = async () => {
    appInfoContext.setLoggedIn(false);
    setHaveAccount(true);
    localStorage.removeItem("jwt");
  }

  return (
    <Grid container xs={8} className="loginMainWrapper">
      <Grid container xs={11} className="loginInnerWrapper">
        <Typography className='loginMainTitle'>Setting - Profile</Typography>
        <Grid xs={12}>
          <SettingsNavBar 
            settingsSelection={settingsSelection}
          />
          <Grid container xs={8} className="loginFormWrapper">
            <Grid container xs={12} className="loginButtonWrapper">
              <Button  onClick={handleSignout} className="logoutButton">
                <Typography className="loginButtonText">SIGN OUT</Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default ProfilePage
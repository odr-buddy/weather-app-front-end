import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AppInfoContext } from '../context/AppInfoContext';
import "../styles/components/Login.css";
import SettingsNavBar from './SettingsNavBar';

function EditLocationLogIn() {
  const appInfoContext = useContext(AppInfoContext);
  const settingsSelection = "locations";

  return (
    <Grid container xs={8} className="loginMainWrapper">
      <Grid container xs={11} className="loginInnerWrapper">
        <Typography className='loginMainTitle'>Setting - Profile</Typography>
        <Grid xs={12}>
          <SettingsNavBar 
            settingsSelection={settingsSelection}
          />
          <Grid container xs={8} className="loginFormWrapper">
            <Grid container xs={12} className="loginButtonWrapper">
            <Link className='settingNavLink' to="/profile">
              <Typography className='editLocationLogin' >Log in to add/edit locations</Typography>
            </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default EditLocationLogIn
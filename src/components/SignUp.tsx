import { Button, Grid, TextField, Typography } from '@mui/material';
import React, {useContext, useState} from 'react'
import "../styles/components/SignUp.css";
import SettingsNavBar from './SettingsNavBar';
import axios from 'axios';
import { AppInfoContext } from '../context/AppInfoContext';

export type SignUpProps = {
  haveAccount: boolean,
  handleHaveAccount: () => void
}

function SignUp({haveAccount, handleHaveAccount}:SignUpProps) {
  const appInfoContext = useContext(AppInfoContext);
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [emailMsg, setEmailMsg] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [passwordMsg, setPasswordMsg] = useState("");
  const [confirmPwd, setConfirmPwd] = useState("");
  const [confirmPwdError, setConfirmPwdError] = useState(false);
  const [confirmPwdMsg, setConfirmPwdMsg] = useState("");
  const [firstName, setFirstName] = useState("");
  const [firstNameError, setFirstNameError] = useState(false);
  const [firstNameMsg, setFirstNameMsg] = useState("");
  const [lastName, setLastName] = useState("");
  const [lastNameError, setLastNameError] = useState(false);
  const [lastNameMsg, setLastNameMsg] = useState("");
  const settingsSelection = "profile";

  const emailChange = (newEmail:any) => {
    setEmail(newEmail.target.value);
  };

  const passwordChange = (newPassword:any) => {
    setPassword(newPassword.target.value);
  };

  const confirmPwdChange = (newConfirmPwd:any) => {
    setConfirmPwd(newConfirmPwd.target.value);
  };

  const fNameChange = (newFName:any) => {
    setFirstName(newFName.target.value);
  };

  const lNameChange = (newLName:any) => {
    setLastName(newLName.target.value);
  };

  const handleSubmit = async () => {
    setEmailError(false);
    setEmailMsg("");
    setPasswordError(false);
    setPasswordMsg("");
    setConfirmPwdError(false);
    setConfirmPwdMsg("");
    setFirstNameError(false);
    setFirstNameMsg("");
    setLastNameError(false);
    setLastNameMsg("");

    if(confirmPwd !== password){
      setConfirmPwdError(true);
      setConfirmPwdMsg("Confirmation Password must match password");
      return;
    }

    if(firstName === ""){
      setFirstNameError(true);
      setFirstNameMsg("This field is required");
      return;
    }

    if(lastName === ""){
      setLastNameError(true);
      setLastNameMsg("This field is required");
      return;
    }

    const response = await axios.post("https://weather-app-auth.uk.r.appspot.com/api/auth/register", {
      name: `${firstName} ${lastName}`,
      email: email,
      password: password
    },
    {
      headers: {
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Credentials': 'true',
      }
    })
    .catch(error => {
      console.log("Error", error);
      if(error.response){
        console.log("Error", error.response.data);
        if(error.response.data.includes('"email"') || error.response.data.includes("email")){
          setEmailError(true);
          setEmailMsg("Invalid email");
        }
        if(error.response.data === "Email already exists"){
          setEmailError(true);
          setEmailMsg("An account already exists for this email" );
        }
        if(error.response.data.includes('"password"') || error.response.data.includes("password")){
          setPasswordError(true);
          setPasswordMsg("Invalid password");
        }
      }
    });

    if(response instanceof Object) {
      localStorage.setItem("jwt", response.data);
      setEmail("");
      setPassword("");
      setConfirmPwd("");
      setFirstName("");
      setLastName("");
      appInfoContext.setLoggedIn(true);
    }
  }

  return (
    <Grid container xs={8} className="signupMainWrapper">
      <Grid container xs={11} className="signupInnerWrapper">
        <Typography className='signupMainTitle'>Setting - Profile</Typography>
        <Grid xs={12}>
          <SettingsNavBar 
            settingsSelection={settingsSelection}
          />
          <Typography className='signupSubTitle'>Sign Up</Typography>
          <Grid container xs={8} className="signupFormWrapper">
            <Typography className="signupFormTitle">Email*</Typography>
            <TextField error={emailError} helperText={emailMsg} fullWidth={true} value={email} onChange={(email:any) => emailChange(email)} className="signupFormTF"></TextField>
            <Typography className="signupFormTitle">Password*</Typography>
            <TextField error={passwordError} helperText={passwordMsg} type="password" fullWidth={true} value={password} onChange={(password:any) => passwordChange(password)} className="signupFormTF"></TextField>
            <Typography className="signupFormTitle">Confirm Password*</Typography>
            <TextField error={confirmPwdError} helperText={confirmPwdMsg} type="password" fullWidth={true} value={confirmPwd} onChange={(confPwd:any) => confirmPwdChange(confPwd)} className="signupFormTF"></TextField>
            <Typography className="signupFormTitle">First Name*</Typography>
            <TextField error={firstNameError} helperText={firstNameMsg} fullWidth={true} value={firstName} onChange={(fName:any) => fNameChange(fName)} className="signupFormTF"></TextField>
            <Typography className="signupFormTitle">Last Name*</Typography>
            <TextField error={lastNameError} helperText={lastNameMsg}fullWidth={true} value={lastName} onChange={(lName:any) => lNameChange(lName)} className="signupFormTF"></TextField>
            <Grid container xs={12} className="signupButtonWrapper">
              <Button onClick={handleSubmit} className="signupButton">
                <Typography className="signupButtonText">CREATE ACCOUNT</Typography>
              </Button>
            </Grid>
            <Grid container xs={12} className="signupButtonWrapper">
              <Button onClick={handleHaveAccount}>
                <Typography className="signupNoAccButtonText">Already have an account? Log in</Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default SignUp
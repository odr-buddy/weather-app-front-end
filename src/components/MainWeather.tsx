import { Grid, Table, TableBody, TableCell, TableRow, Typography } from '@mui/material'
import React, {useState, useEffect, useContext} from 'react'
import { AppInfoContext } from '../context/AppInfoContext'
import { CityInfoModel } from '../routes/Home'
import "../styles/components/MainWeather.css"

function MainWeather() {
  const appInfoContext = useContext(AppInfoContext);
  const [windDegree, setWindDegree] = useState("N");
  const [sunrise, setSunrise] = useState("");
  const [sunset, setSunset] = useState("");


  useEffect( () => {
    if(appInfoContext.weatherInfo !== null){
      if(appInfoContext.weatherInfo.current.wind_deg === 0 || appInfoContext.weatherInfo.current.wind_deg === 360){
        setWindDegree("N");
      } else if(appInfoContext.weatherInfo.current.wind_deg > 0 && appInfoContext.weatherInfo.current.wind_deg < 90){
        setWindDegree("NE");
      } else if(appInfoContext.weatherInfo.current.wind_deg === 90){
        setWindDegree("E");
      } else if(appInfoContext.weatherInfo.current.wind_deg > 90 && appInfoContext.weatherInfo.current.wind_deg < 180){
        setWindDegree("SE");
      } else if(appInfoContext.weatherInfo.current.wind_deg === 180){
        setWindDegree("S");
      } else if(appInfoContext.weatherInfo.current.wind_deg > 180 && appInfoContext.weatherInfo.current.wind_deg < 270){
        setWindDegree("SW");
      } else if(appInfoContext.weatherInfo.current.wind_deg === 270){
        setWindDegree("W");
      } else if(appInfoContext.weatherInfo.current.wind_deg > 270 && appInfoContext.weatherInfo.current.wind_deg < 360){
        setWindDegree("NW");
      }

      const tempRiseDate = new Date(appInfoContext.weatherInfo.current.sunrise);
      setSunrise(tempRiseDate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }));

      const tempSetDate = new Date(appInfoContext.weatherInfo.current.sunset);
      setSunset(tempSetDate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }));
    }
  }, [appInfoContext.weatherInfo]);

  return (
    <Grid container xs={8} className="mainWeatherWrapper">
      <Grid container xs={11} className="mainWeatherInfoContainer">
        <Grid container xs={5.8} className="mainWeatherHighlightInfo">
          {
            appInfoContext.currentCity.state != undefined ?
            <Typography className="mainCityText">{`${appInfoContext.currentCity.name}, ${appInfoContext.currentCity.state}`}</Typography>:
            <Typography className="mainCityText">{`${appInfoContext.currentCity.name}, ${appInfoContext.currentCity.country}`}</Typography>
          }
          
          <Grid xs={12} container className="mainWeaterImageContainer">
            {
              appInfoContext.weatherInfo === null ? null : 
              appInfoContext.weatherInfo.current.weather[0].main === "Thunderstorm" ?
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649288739/WeatherApp/thunderstorm_l8zzyb.png" /> :
              appInfoContext.weatherInfo.current.weather[0].main === "Drizzle" ?
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /> :
              appInfoContext.weatherInfo.current.weather[0].main === "Rain" ?
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /> :
              appInfoContext.weatherInfo.current.weather[0].main === "Snow" ?
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/snow_q0f5zw.png" /> :
              appInfoContext.weatherInfo.current.weather[0].main === "Clear" ?
              <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/sunny_yacgl0.png" /> :
              appInfoContext.weatherInfo.current.weather[0].main === "Clouds" ?
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/clouds_hpmqrh.png" /> :
                <img src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/haze_b2s3ly.png" /> 
            }
          </Grid>
          <Grid container xs={12} className="mainWeatherCondContainer">
            <Typography className="mainWeatherCondText">{appInfoContext.weatherInfo === null ? null : appInfoContext.weatherInfo.current.weather[0].main}</Typography>
          </Grid>
          <Grid container xs={12} className="mainWeatherTempContainer">
            <Grid container xs={6} className="mainWeatherTempLeft">
              {
                appInfoContext.tempScale === "metric" ?
                  <Typography className="mainWeatherTempText">{appInfoContext.weatherInfo === null ? null : `${Math.round(appInfoContext.weatherInfo.current.temp)}°C`}</Typography> :
                  <Typography className="mainWeatherTempText">{appInfoContext.weatherInfo === null ? null : `${Math.round(appInfoContext.weatherInfo.current.temp)}°F`}</Typography>
              }
            </Grid>
            <Grid container xs={6} className="mainWeatherTempRight">
              <Typography className="mainWeatherFeelsText">Feels Like</Typography>
              {
                appInfoContext.tempScale === "metric" ?
                  <Typography className="mainWeatherTempText">{appInfoContext.weatherInfo === null ? null : `${Math.round(appInfoContext.weatherInfo.current.feels_like)}°C`}</Typography> :
                  <Typography className="mainWeatherTempText">{appInfoContext.weatherInfo === null ? null : `${Math.round(appInfoContext.weatherInfo.current.feels_like)}°F`}</Typography>
              }
            </Grid>
          </Grid>
        </Grid>
        <Grid container xs={5.8} className="mainWeatherSideInfo">
          <Table style={{width: "90%", marginLeft: 'auto', marginRight: 'auto', marginTop: "8px", marginBottom: "8px"}}>
            <TableBody>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">Wind Speed</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{appInfoContext.weatherInfo === null ? null : `${appInfoContext.weatherInfo.current.wind_speed}`}</Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">Wind Degree</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{windDegree}</Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">Humidity</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{appInfoContext.weatherInfo === null ? null : `${appInfoContext.weatherInfo.current.humidity}%`}</Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">UV Index</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{appInfoContext.weatherInfo === null ? null : `${appInfoContext.weatherInfo.current.uvi}`}</Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  {
                    appInfoContext.weatherInfo === null ?  <Typography className="tableText">Rain Past 1H</Typography> :
                    appInfoContext.weatherInfo.current.snow !== undefined ?
                    <Typography className="tableText">Snow Past 1H</Typography> :
                    <Typography className="tableText">Rain Past 1H</Typography>
                  }
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">
                  {
                    appInfoContext.weatherInfo === null ? "0 mm" :
                    appInfoContext.weatherInfo.current.snow !== undefined ? `${appInfoContext.weatherInfo.current.snow["1h"]} mm` :
                    appInfoContext.weatherInfo.current.rain !== undefined ? `${appInfoContext.weatherInfo.current.rain["1h"]} mm` :
                    "0 mm"
                  }
                  </Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">Sunrise</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{sunrise}</Typography>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align='center' style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">Sunset</Typography>
                </TableCell>
                <TableCell style={{borderBottom:"none", color: "#E0FBFC"}}>
                  <Typography className="tableText">{sunset}</Typography>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default MainWeather
import { Box, Button, Grid, Modal, TextField, Typography } from '@mui/material'
import React, { useContext, useState } from 'react'
import { AppInfoContext } from '../context/AppInfoContext';
import "../styles/components/NoLocationsModal.css";
import SettingsNavBar from './SettingsNavBar';

export type NoLocationProps = {
  noLocations: boolean
  setNoLocations: (bool:any) => void
}

function NoLocationsModal({setNoLocations, noLocations}:NoLocationProps) {
  const appInfoContext = useContext(AppInfoContext);
  const settingsSelection = "profile";

  const handleClose = () => {
    setNoLocations(false);
  }

  return (
    <Modal
        open={noLocations}
        onClose={handleClose}
      >
        <Box className='NLMainBox'>
          <Typography className='NLMainText'>
            There are no locations with this name
          </Typography>
          <Button className='NLButton' onClick={handleClose}>
            <Typography className='NLButtonText'>Ok</Typography>
          </Button>
        </Box>
      </Modal>
  )
}

export default NoLocationsModal
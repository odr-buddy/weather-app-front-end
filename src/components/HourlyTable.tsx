import { Button, Grid, Typography } from '@mui/material'
import React, { useContext, useState, useEffect } from 'react'
import "../styles/components/HourlyTable.css"
import { AppInfoContext } from '../context/AppInfoContext'

type HourlyTemplate = {
  day: any[],
  time: any[],
  desc: any[],
  img: any[],
  temp: any[],
  feels: any[],
  pop: any[]
}

function HourlyTable() {
  const appInfoContext = useContext(AppInfoContext);
  const [hourly, setHourly] = useState<HourlyTemplate>();
  const [currentHours, setCurrentHours] = useState(0);
  const [newHours, setNewHours] = useState(false);

  useEffect( () => {
    setTableView();
  }, [appInfoContext.weatherInfo]);

  useEffect( () => {
    if(newHours === true){
      setTableView();
    }
  }, [currentHours]);

  const handlePrev = () => {
    if(currentHours > 0){
      setNewHours(true);
      setCurrentHours(currentHours-1);
    }
  }

  const handleNext = () => {
    if(currentHours < 5){
      setNewHours(true);
      setCurrentHours(currentHours+1);
    }
  }

  const setTableView = () => {
    if(appInfoContext.weatherInfo !== null) {
      if(appInfoContext.weatherInfo.hourly !== undefined) {
        const tableInfo:HourlyTemplate = {
          day: [<td className='weatherSideText'></td>],
          time: [<td className='weatherSideText'></td>],
          desc: [<td className='weatherSideText'></td>],
          img: [<td className='weatherSideText'></td>],
          temp: [<td className='weatherSideText'></td>],
          feels: [<td className='weatherSideText' style={{paddingBottom: '10px'}}>Feels like</td>],
          pop: [<td className='weatherSideText' style={{paddingBottom: '10px'}}>POP</td>]
        }
  
        for(let i =  8 *(currentHours); i < 8 *(currentHours + 1); i++){
          const day = new Date(appInfoContext.weatherInfo.hourly[i].dt * 1000).getDay();
          const DOW = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
          if(appInfoContext.tempScale === "metric"){
            tableInfo.day.push(<td className='weatherDayText'>{DOW[day]}</td>);
            tableInfo.time.push(<td className='weatherTextSmall' style={{paddingBottom: '10px'}}>{new Date(appInfoContext.weatherInfo.hourly[i].dt * 1000).toLocaleTimeString('en-US', {hour12:true,hour:'numeric'})}</td>);
            tableInfo.desc.push(<td className='weatherTextSmall'>{appInfoContext.weatherInfo.hourly[i].weather[0].main}</td>);
            switch(appInfoContext.weatherInfo.hourly[i].weather[0].main) { 
              case "Thunderstorm": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649288739/WeatherApp/thunderstorm_l8zzyb.png" /></td>);
                 break; 
              } 
              case "Drizzle": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Rain": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Snow": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/snow_q0f5zw.png" /></td>); 
                 break; 
              } 
              case "Clear": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/sunny_yacgl0.png" /></td>);
                 break; 
              } 
              case "Clouds": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/clouds_hpmqrh.png" /></td>);
                 break; 
              } 
              default: { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/haze_b2s3ly.png" /></td>); 
                 break; 
              } 
           } 
            tableInfo.temp.push(<td className='weatherTempText'>{`${Math.round(appInfoContext.weatherInfo.hourly[i].temp)}°C`}</td>);
            tableInfo.feels.push(<td  className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(appInfoContext.weatherInfo.hourly[i].feels_like)}°C`}</td>);
            tableInfo.pop.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(appInfoContext.weatherInfo.hourly[i].pop)}%`}</td>);
          } else {
            tableInfo.day.push(<td className='weatherDayText'>{DOW[day]}</td>);
            tableInfo.time.push(<td className='weatherTextSmall' style={{paddingBottom: '10px'}}>{new Date(appInfoContext.weatherInfo.hourly[i].dt * 1000).toLocaleTimeString('en-US', {hour12:true,hour:'numeric'})}</td>);
            tableInfo.desc.push(<td className='weatherTextSmall'>{appInfoContext.weatherInfo.hourly[i].weather[0].main}</td>);
            switch(appInfoContext.weatherInfo.hourly[i].weather[0].main) { 
              case "Thunderstorm": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649288739/WeatherApp/thunderstorm_l8zzyb.png" /></td>);
                 break; 
              } 
              case "Drizzle": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Rain": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Snow": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/snow_q0f5zw.png" /></td>); 
                 break; 
              } 
              case "Clear": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/sunny_yacgl0.png" /></td>);
                 break; 
              } 
              case "Clouds": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/clouds_hpmqrh.png" /></td>);
                 break; 
              } 
              default: { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/haze_b2s3ly.png" /></td>); 
                 break; 
              } 
           } 
            tableInfo.temp.push(<td className='weatherTempText'>{`${Math.round(appInfoContext.weatherInfo.hourly[i].temp)}°F`}</td>);
            tableInfo.feels.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(appInfoContext.weatherInfo.hourly[i].feels_like)}°F`}</td>);
            tableInfo.pop.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(appInfoContext.weatherInfo.hourly[i].pop)}%`}</td>);
          }
        }
        setHourly(tableInfo);
      }
    }
    
    console.log("Weather Info", appInfoContext.weatherInfo);
  };

  return (
    <Grid container xs={8} className="hourlyTableMainWrapper">
      <Grid container xs={12}>
        <Grid container xs={6} className="hourlyTitleContLeft">
          <Typography className='hourlyTitleText'>Hourly Forecast</Typography>
          <Typography className='hourlySubTitleText'>24 Hours</Typography>
        </Grid>
        <Grid container xs={6} className="hourlyTitleContRight">
          <Button onClick={handlePrev}>
            <Typography className='hourlyPrevText'>{"< PREV"}</Typography>
          </Button>
          <Button onClick={handleNext}>
            <Typography className='hourlyNextText'>{"NEXT >"}</Typography>
          </Button>
        </Grid>
      </Grid>
      {hourly === undefined ? null :
      <table style={{width: '100%', borderCollapse: 'collapse'}}>
        <tr style={{border: 'none'}}>
          {hourly.day}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.time}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.desc}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.img}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.temp}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.feels}
        </tr>
        <tr style={{border: 'none'}}>
          {hourly.pop}
        </tr>
      </table>}
    </Grid>
  )
}

export default HourlyTable
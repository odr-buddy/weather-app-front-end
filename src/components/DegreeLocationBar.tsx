import { Grid, TextField, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles';
import React, { useContext } from 'react'
import { AppInfoContext } from '../context/AppInfoContext'
import "../styles/components/DegreeLocationBar.css"

const useStyles = makeStyles({
  root: {
    '& .MuiInputBase-input': {
      color: '#e0fbfc', // Text color
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#e0fbfc', // Semi-transparent underline
    },
    '& .MuiInput-underline:hover:before': {
      borderBottomColor: '#e0fbfc', // Solid underline on hover
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#e0fbfc', // Solid underline on focus
    },
  },
});

function DegreeLocationBar() {
  const appInfoContext = useContext(AppInfoContext);
  const classes = useStyles();
  
  const handleKeyDown = (event:any) => {
    if(event.code === "Enter"){
      appInfoContext.handleCitySubmit();
    }
  }
  
  return (
    <Grid container xs={8} className="mainWrapper">
      <Grid container xs={11} className="contentWrapper">
        <Grid container xs={6} className="tempScaleWrapper">
          <Grid className="leftText" onClick={() => appInfoContext.handleTempScaleChange("metric")}>
            <Typography className={appInfoContext.tempScale === "metric" ? "selectedText" : "notSelectedText"}>C°</Typography>
          </Grid>
          <Grid className="rightText" onClick={() => appInfoContext.handleTempScaleChange("imperial")}>
            <Typography className={appInfoContext.tempScale === "imperial" ? "selectedText" : "notSelectedText"}>F°</Typography>
          </Grid>
        </Grid>
        <Grid container xs={6} className="cityInputWrapper">
          <TextField 
            id="standard-basic" 
            placeholder='Search for locations'
            variant="standard"
            className={classes.root}
            value={appInfoContext.city}
            onChange={(text:any) => appInfoContext.handleCityChange(text)}
            onKeyDown={(event:any) => handleKeyDown(event)}
            onSubmit={appInfoContext.handleCitySubmit}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default DegreeLocationBar
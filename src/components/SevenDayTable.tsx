import { Grid, Typography } from '@mui/material'
import React, { useEffect, useContext, useState } from 'react'
import "../styles/components/SevenDayTable.css"
import { AppInfoContext } from '../context/AppInfoContext'

type SevenDayTemplate = {
  day: any[],
  desc: any[],
  img: any[],
  temp: any[],
  feels: any[],
  night: any[],
  pop: any[]
}

function SevenDayTable() {
  const appInfoContext = useContext(AppInfoContext);
  const [sevenDay, setSevenDay] = useState<SevenDayTemplate>();

  useEffect( () => {
    if(appInfoContext.weatherInfo !== null) {
      if(appInfoContext.weatherInfo.daily !== undefined) {
        const tableInfo:SevenDayTemplate = {
          day: [<td className='weatherSideText'></td>],
          desc: [<td className='weatherSideText'></td>],
          img: [<td className='weatherSideText'></td>],
          temp: [<td className='weatherSideText'></td>],
          feels: [<td className='weatherSideText' style={{paddingBottom: '10px'}}>Feels like</td>],
          night: [<td className='weatherSideText' style={{paddingBottom: '10px'}}>Night</td>],
          pop: [<td className='weatherSideText' style={{paddingBottom: '10px'}}>POP</td>]
        }
        appInfoContext.weatherInfo.daily.forEach((element:any) => {
          const day = new Date(element.dt * 1000).getDay();
          const DOW = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
          if(appInfoContext.tempScale === "metric"){
            tableInfo.day.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{DOW[day]}</td>);
            tableInfo.desc.push(<td className='weatherTextSmall'>{element.weather[0].main}</td>);
            switch(element.weather[0].main) { 
              case "Thunderstorm": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649288739/WeatherApp/thunderstorm_l8zzyb.png" /></td>);
                 break; 
              } 
              case "Drizzle": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Rain": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Snow": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/snow_q0f5zw.png" /></td>); 
                 break; 
              } 
              case "Clear": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/sunny_yacgl0.png" /></td>);
                 break; 
              } 
              case "Clouds": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/clouds_hpmqrh.png" /></td>);
                 break; 
              } 
              default: { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/haze_b2s3ly.png" /></td>); 
                 break; 
              } 
           }
            tableInfo.temp.push(<td className='weatherTempText'>{`${Math.round(element.temp.day)}°C`}</td>);
            tableInfo.feels.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.feels_like.day)}°C`}</td>);
            tableInfo.night.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.temp.night)}°C`}</td>);
            tableInfo.pop.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.pop)}%`}</td>);
          } else {
            tableInfo.day.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{DOW[day]}</td>);
            tableInfo.desc.push(<td className='weatherTextSmall'>{element.weather[0].main}</td>);
            switch(element.weather[0].main) { 
              case "Thunderstorm": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649288739/WeatherApp/thunderstorm_l8zzyb.png" /></td>);
                 break; 
              } 
              case "Drizzle": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Rain": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/slight-rain_iylqwv.png" /></td>);
                 break; 
              } 
              case "Snow": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/snow_q0f5zw.png" /></td>); 
                 break; 
              } 
              case "Clear": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289298/WeatherApp/sunny_yacgl0.png" /></td>);
                 break; 
              } 
              case "Clouds": { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/clouds_hpmqrh.png" /></td>);
                 break; 
              } 
              default: { 
                tableInfo.img.push(<td className='weatherImgTd'><img className='weatherimg' src="https://res.cloudinary.com/dgjb93yr6/image/upload/v1649289297/WeatherApp/haze_b2s3ly.png" /></td>); 
                 break; 
              } 
           }
            tableInfo.temp.push(<td className='weatherTempText'>{`${Math.round(element.temp.day)}°F`}</td>);
            tableInfo.feels.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.feels_like.day)}°F`}</td>);
            tableInfo.night.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.temp.night)}°F`}</td>);
            tableInfo.pop.push(<td className='weatherDayText' style={{paddingBottom: '10px'}}>{`${Math.round(element.pop)}%`}</td>);
          }
          
          
        });
        setSevenDay(tableInfo);
      }
    }
    
    console.log("Weather Info", appInfoContext.weatherInfo);
  }, [appInfoContext.weatherInfo]);

  return (
    <Grid container xs={8} className="sevenTableMainWrapper">
      <Grid container xs={12}>
        <Typography className='sevenTitleText'>7 Day Forecast</Typography>
      </Grid>
      {sevenDay === undefined ? null :
      <table style={{width: '100%', borderCollapse: 'collapse'}}>
        <tr style={{border: 'none'}}>
          {sevenDay.day}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.desc}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.img}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.temp}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.feels}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.night}
        </tr>
        <tr style={{border: 'none'}}>
          {sevenDay.pop}
        </tr>
      </table>}
    </Grid>
  )
}

export default SevenDayTable
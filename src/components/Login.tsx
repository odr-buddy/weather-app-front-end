import { Button, Grid, TextField, Typography, useForkRef } from '@mui/material'
import React, { useContext, useState } from 'react'
import { AppInfoContext } from '../context/AppInfoContext';
import "../styles/components/Login.css";
import SettingsNavBar from './SettingsNavBar';
import axios from 'axios';
import { CityInfoModel } from '../routes/Home';

export type LoginProps = {
  haveAccount: boolean,
  handleHaveAccount: () => void
}

function Login({haveAccount, handleHaveAccount}:LoginProps) {
  const appInfoContext = useContext(AppInfoContext);
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [emailMsg, setEmailMsg] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [passwordMsg, setPasswordMsg] = useState("");
  const settingsSelection = "profile";

  const emailChange = (newEmail:any) => {
    setEmail(newEmail.target.value);
  };

  const passwordChange = (newPassword:any) => {
    setPassword(newPassword.target.value);
  };

  const handleSubmit = async () => {
    setEmailError(false);
    setEmailMsg("");
    setPasswordError(false);
    setPasswordMsg("");
    const response = await axios.post("https://weather-app-auth.uk.r.appspot.com/api/auth/login", {
      email: email,
      password: password
    },
    {
      headers: {
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Credentials': 'true',
      }
    })
    .catch((error) => {
        console.log("Error", error);
        if(error.response){
          console.log("Error", error.response.data);
          if(error.response.data === "This user email does not exist" || error.response.data.includes('"email"') || error.response.data.includes("email")){
            setEmailError(true);
            setEmailMsg("This user email does not exist");
          }
          if(error.response.data === "Invalid password" || error.response.data.includes('"password"') || error.response.data.includes("password")){
            setPasswordError(true);
            setPasswordMsg("Invalid password");
          }
        }
    });

    if(response instanceof Object) {
      localStorage.setItem("jwt", response.data);
      setEmail("");
      setPassword("");

      axios.get("https://weather-app-auth.uk.r.appspot.com/api/location",
      {
        headers: {
          "authtoken": response.data,
          "Access-Control-Allow-Origin": "*",
          'Access-Control-Allow-Credentials': 'true',
        }
      }).then((result) => {
        appInfoContext.setSavedLocation(result.data.location.sort((a: any, b: any) => (a.city < b.city ? -1 : 1)));
        const favourite = result.data.location.find((obj: any) => {return obj.favourite === true});

        var cityModel: CityInfoModel = {
          country: favourite.country,
          state: favourite.state,
          name: favourite.city,
          lat: favourite.lat,
          long: favourite.long,
        }
        appInfoContext.setCurrentCity(cityModel);
      }).catch((error) => {
        console.log("Error", error);
      });

      appInfoContext.setLoggedIn(true);
    }
    
  }

  return (
    <Grid container xs={8} className="loginMainWrapper">
      <Grid container xs={11} className="loginInnerWrapper">
        <Typography className='loginMainTitle'>Setting - Profile</Typography>
        <Grid xs={12}>
          <SettingsNavBar 
            settingsSelection={settingsSelection}
          />
          <Typography className='loginSubTitle'>Login</Typography>
          <Grid container xs={8} className="loginFormWrapper">
            <Typography className="loginFormTitle">Email</Typography>
            <TextField error={emailError} helperText={emailMsg} fullWidth={true} value={email} onChange={(email:any) => emailChange(email)} className="loginFormTF"></TextField>
            <Typography className="loginFormTitle">Password</Typography>
            <TextField type="password" error={passwordError} helperText={passwordMsg} fullWidth={true} value={password} onChange={(password:any) => passwordChange(password)} className="loginFormTF"></TextField>
            <Grid container xs={12} className="loginButtonWrapper">
              <Button  onClick={handleSubmit} className="loginButton">
                <Typography className="loginButtonText">LOG IN</Typography>
              </Button>
            </Grid>
            <Grid container xs={12} className="loginButtonWrapper">
              <Button onClick={handleHaveAccount}>
                <Typography className="loginNoAccButtonText">Don't have an account? Sign up</Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Login
import { Grid, TextField, Typography } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'
import "../styles/components/SettingsNavBar.css"

export type SettingsNavBarProps = {
  settingsSelection: string
}

function SettingsNavBar({settingsSelection}:SettingsNavBarProps) {
  return (
    <Grid container xs={12} className="settingNavMainWrapper">
      <Grid container xs={11} className="settingsNavContentWrapper">
        <Grid container xs={12}>
          <Grid className="settingsNavLeftText">
            <Link className='settingNavLink' to="/profile">
              <Typography className={settingsSelection === "profile" ? "settingsNavSelected" : "settingsNavNotSelected"}>MY PROFILE</Typography>
            </Link>
          </Grid>
          <Grid className="settingsNavRightText">
            <Link className='settingNavLink' to="/edit-locations">
              <Typography className={settingsSelection === "locations" ? "settingsNavSelected" : "settingsNavNotSelected"}>EDIT LOCATIONS</Typography>
            </Link>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default SettingsNavBar
import { Button, Grid, IconButton, ListItemButton, ListItemText, Typography } from '@mui/material'
import { useContext, useEffect, useState } from 'react'
import { AppInfoContext } from '../context/AppInfoContext';
import "../styles/components/Login.css";
import "../styles/components/EditLocations.css";
import SettingsNavBar from './SettingsNavBar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import DeleteIcon from '@mui/icons-material/Delete';
import { CityInfoModel } from "../routes/Home";
import { useNavigate } from 'react-router-dom'
import axios from 'axios';

function EditLocationComp() {
  const appInfoContext = useContext(AppInfoContext);
  const settingsSelection = "locations";
  const navigate = useNavigate();
  const [favouriteToggled, setFavouriteToggled] = useState(false);
  const [newLocationAdded, setNewLocationAdded] = useState(false);

  const getSavedLocationsList = (token:any) => {
    axios.get("https://weather-app-auth.uk.r.appspot.com/api/location",
    {
      headers: {
        "authtoken": token,
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Credentials': 'true',
      }
    }).then((result) => {
      appInfoContext.setSavedLocation(result.data.location.sort((a: any, b: any) => (a.city < b.city ? -1 : 1)));
    }).catch((error) => {
      console.log("Error", error);
    });
  }

  const updateLocations = (token: any, locationData: any[]) => {
    const data = { "location": locationData };

    axios.put("https://weather-app-auth.uk.r.appspot.com/api/location", data,
    {
      headers: {
        "authtoken": token,
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Credentials': 'true',
      }
    }).catch((error) => {
      console.log("Error", error);
    });
  }

  const favouriteIcon = (isFavourite: Boolean) => {
    if (isFavourite) {
      return <FavoriteIcon/>
    } else {
      return <FavoriteBorderIcon/>
    }
  }

  const setCurrentLocation = (location: any) => {
    var cityModel: CityInfoModel = {
      country: location.country,
      state: location.state,
      name: location.city,
      lat: location.lat,
      long: location.long,
    }
    appInfoContext.setCurrentCity(cityModel);
  }

  const selectLocation = (selectedID: string) => () => {
    const userSelect = appInfoContext.savedLocations.find(obj => {return obj._id === selectedID})
    setCurrentLocation(userSelect);
    appInfoContext.setTimeLength("hourly")
    navigate("/");
  }

  const deleteLocation = (selectedID: string) => () => {
    let filteredLocations = appInfoContext.savedLocations.filter(obj => {return obj._id !== selectedID});
    appInfoContext.setSavedLocation(filteredLocations);
    updateLocations(localStorage.getItem("jwt"), filteredLocations);
  }

  const toggleFavourite = (selectedID: string) => () => {
    const selectedIndex = appInfoContext.savedLocations.findIndex(obj => {return obj._id === selectedID})
    const oldFavouriteLocationIndex = appInfoContext.savedLocations.findIndex(obj => {return obj.favourite === true})

    if (oldFavouriteLocationIndex >= 0) {
      appInfoContext.savedLocations[oldFavouriteLocationIndex].favourite = false;
    }
    appInfoContext.savedLocations[selectedIndex].favourite = appInfoContext.savedLocations[selectedIndex].favourite ? false : true;

    updateLocations(localStorage.getItem("jwt"), appInfoContext.savedLocations);

    const favourite = appInfoContext.savedLocations.find((obj: any) => {return obj.favourite === true});
    setCurrentLocation(favourite);
    
    setFavouriteToggled(!favouriteToggled);
  }

  const saveCurrentLocation = () => () => {
    const newLocation = {
      country: appInfoContext.currentCity.country,
      state: appInfoContext.currentCity.state,
      city: appInfoContext.currentCity.name,
      lat: appInfoContext.currentCity.lat,
      long: appInfoContext.currentCity.long,
      favourite: false
    }
    appInfoContext.savedLocations.push(newLocation);
    appInfoContext.savedLocations.sort((a: any, b: any) => (a.city < b.city ? -1 : 1));

    updateLocations(localStorage.getItem("jwt"), appInfoContext.savedLocations);

    setNewLocationAdded(!newLocationAdded);
  }

  const locationList = (locations: Array<any>) => {
    const listItems = locations.map((location) =>
      <ListItem key={location._id}>
        <ListItemButton onClick={selectLocation(location._id)}>
          <ListItemText primary={location.city} secondary={location.state + ", " + location.country}/>
        </ListItemButton>
        <IconButton edge="end" onClick={toggleFavourite(location._id)}>
            {favouriteIcon(location.favourite)}
        </IconButton>
        <IconButton edge="end" onClick={deleteLocation(location._id)}>
            <DeleteIcon/>
        </IconButton>
      </ListItem>
    );
    return (
      <List>{listItems}</List>
    );
  }

  useEffect( () => {
    if (appInfoContext.loggedIn) {
      getSavedLocationsList(localStorage.getItem("jwt"));
    }
  }, []);

  return (
    <Grid container xs={8} className="editMainWrapper">
      <Grid container xs={11} className="editInnerWrapper">
        <Typography className='editMainTitle'>Setting - Edit Locations</Typography>
        <Grid xs={12}>
          <SettingsNavBar 
            settingsSelection={settingsSelection}
          />
          <Grid container xs={12} className="editListWrapper">
            <Grid container xs={12} className="editButtonWrapper">
              <Button className="editButton" onClick={saveCurrentLocation()}>
                <Typography className="editButtonText">Save Current Location</Typography>
              </Button>
            </Grid>
          </Grid>
          {locationList(appInfoContext.savedLocations)}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default EditLocationComp
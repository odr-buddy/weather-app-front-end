import { Grid, Typography } from '@mui/material'
import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react'
import HourlyTable from '../components/HourlyTable';
import MainWeather from '../components/MainWeather';
import { AppInfoContext } from '../context/AppInfoContext';

export type CityInfoModel = {
  country: string,
  state: string,
  name: string,
  lat: number,
  long: number
}

function Home() {

  const appInfoContext = useContext(AppInfoContext);

  useEffect( () => {
    axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${appInfoContext.currentCity.lat}&lon=${appInfoContext.currentCity.long}&exclude=minutely,daily&units=${appInfoContext.tempScale}&appid=fa2fc5f8dbe30ca64561348adad952fa`)
    .then(function (response) {
      console.log("CURR CITY", response);
      appInfoContext.setWeatherInfo(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
  }, [appInfoContext.currentCity, appInfoContext.tempScale]);

  return (
    <Grid xs={12}>
      <MainWeather />

      <HourlyTable />
    </Grid>
  )
}

export default Home
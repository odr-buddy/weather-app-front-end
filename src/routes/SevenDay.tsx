import { Grid } from '@mui/material';
import React, {useState, useEffect, useContext} from 'react'
import MainWeather from '../components/MainWeather';
import SevenDayTable from '../components/SevenDayTable';
import axios from 'axios';
import { AppInfoContext } from '../context/AppInfoContext';

function SevenDay() {
  const appInfoContext = useContext(AppInfoContext);

  useEffect( () => {
    axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${appInfoContext.currentCity.lat}&lon=${appInfoContext.currentCity.long}&exclude=minutely,hourly&units=${appInfoContext.tempScale}&appid=fa2fc5f8dbe30ca64561348adad952fa`)
    .then(function (response) {
      console.log("CURR CITY", response);
      appInfoContext.setWeatherInfo(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
  }, [appInfoContext.currentCity, appInfoContext.tempScale]);

  return (
    <Grid xs={12}>
      <MainWeather />
      <SevenDayTable />
    </Grid>
  )
}

export default SevenDay
import { Grid, Typography } from '@mui/material';
import React, {useContext, useEffect, useState} from 'react'
import EditLocationComp from '../components/EditLocationComp';
import EditLocationLogIn from '../components/EditLocationLogIn';
import { AppInfoContext } from '../context/AppInfoContext';

function EditLocations() {
  const appInfoContext = useContext(AppInfoContext);
  
  return (
    <Grid xs={12}>
      {
        appInfoContext.loggedIn === true ?
        <EditLocationComp /> :
        <EditLocationLogIn />
      }
    </Grid>
  )
}

export default EditLocations
import { Grid, Typography } from '@mui/material';
import React, {useState, useEffect, useContext} from 'react'
import Login from '../components/Login';
import ProfilePage from '../components/ProfilePage';
import SignUp from '../components/SignUp';
import { AppInfoContext } from '../context/AppInfoContext';

function Profile() {
  const appInfoContext = useContext(AppInfoContext);

  const [haveAccount, setHaveAccount] = useState(true);

  const handleHaveAccount = () => {
    if(haveAccount === true) {
      setHaveAccount(false);
    } else {
      setHaveAccount(true);
    }
  }

  return (
    <Grid xs={12} style={{marginBottom: '24px'}}>
      {
        appInfoContext.loggedIn === true ?
        <ProfilePage 
        setHaveAccount={setHaveAccount}
        /> :
        haveAccount === true ?
        <Login
          haveAccount={haveAccount}
          handleHaveAccount={handleHaveAccount}
        /> :
        <SignUp 
          haveAccount={haveAccount}
          handleHaveAccount={handleHaveAccount}
        />
      }
    </Grid>
  )
}

export default Profile
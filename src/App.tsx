import React, { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import './styles/App.css';
import Home, { CityInfoModel } from './routes/Home';
import SevenDay from './routes/SevenDay';
import Profile from './routes/Profile';
import EditLocations from './routes/EditLocations';
import { AppInfoContext } from './context/AppInfoContext';
import axios from 'axios';
import TitleSignInBar from './components/TitleSignInBar';
import DegreeLocationBar from './components/DegreeLocationBar';
import LengthBar from './components/LengthBar';
import { Box, Grid, Modal, Typography } from '@mui/material';
import NoLocationsModal from './components/NoLocationsModal';

function App() {
  const [tempScale, setTempScale] = useState("metric");
  const [city, setCity] = useState("");
  const [timeLength, setTimeLength] = useState("hourly");
  const [currentCity, setCurrentCity] = useState<CityInfoModel>({
    country: "CA", 
    state: "Ontario", 
    name: "Toronto",
    lat: 43.6534817,
    long: -79.3839347
  });
  const [weatherInfo, setWeatherInfo] = useState(null);
  const [multipleCities, setMultipleCities] = useState<CityInfoModel[]>([]);
  const [multipleCitiesModal, setMultipleCitiesModal] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);
  const [savedLocations, setSavedLocation] = useState(Array<JSON>());

  const [noLocations, setNoLocations] = useState(false);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  useEffect( () => {
    const jwt = localStorage.getItem("jwt");
    if(jwt) {
      setLoggedIn(true);

      axios.get("https://weather-app-auth.uk.r.appspot.com/api/location",
      {
        headers: {
          "authtoken": jwt,
          "Access-Control-Allow-Origin": "*",
          'Access-Control-Allow-Credentials': 'true',
        }
      }).then((result) => {
        setSavedLocation(result.data.location.sort((a: any, b: any) => (a.city < b.city ? -1 : 1)));
        const favourite = result.data.location.find((obj: any) => {return obj.favourite === true});

        var cityModel: CityInfoModel = {
          country: favourite.country,
          state: favourite.state,
          name: favourite.city,
          lat: favourite.lat,
          long: favourite.long,
        }
        setCurrentCity(cityModel);
      }).catch((error) => {
        console.log("Error", error);
      });
    }
  }, []);

  const handleTempScaleChange = (newScale:string) => {
    setTempScale(newScale);
  }

  const handleCityChange = (text:any) => {
    setCity(text.target.value);
  }


  const handleSelectCity = (city:CityInfoModel) => {
    setCurrentCity(city);
    setCity("");
    handleClose();
  }

  const handleCitySubmit = () => {
    console.log("IN SUBMIT")
    if(city === ""){
      //error
    }
    

    axios.get(`https://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=5&appid=fa2fc5f8dbe30ca64561348adad952fa`)
    .then(function (response) {
      console.log(response);
      if(response.data.length > 1){
        handleOpen();
        const tempRegions: CityInfoModel[] = [];
        response.data.forEach((element:any) => {
          const newRegion: CityInfoModel = {
            country: element.country, 
            state: element.state, 
            name: element.name,
            lat: element.lat,
            long: element.lon
          };
          tempRegions.push(newRegion);
        });

        setMultipleCities(tempRegions);
      } else if (response.data.length <= 0 ){
        //Show error modal
        setNoLocations(true);
      } else {
        //get the lat and long of that city to be used later
        setCurrentCity({
          country: response.data[0].country, 
          state: response.data[0].state, 
          name: response.data[0].name,
          lat: response.data[0].lat,
          long: response.data[0].lon
        });
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  return (
    <div className="App">
      <AppInfoContext.Provider value={{
        tempScale, 
        setTempScale, 
        city, 
        setCity, 
        timeLength, 
        setTimeLength, 
        currentCity, 
        setCurrentCity, 
        weatherInfo, 
        setWeatherInfo,
        multipleCities,
        setMultipleCities,
        multipleCitiesModal,
        setMultipleCitiesModal,
        handleTempScaleChange,
        handleCityChange,
        handleCitySubmit,
        loggedIn,
        setLoggedIn,
        savedLocations,
        setSavedLocation
      }} >
        <TitleSignInBar />
        <DegreeLocationBar />
        <LengthBar />
        <NoLocationsModal noLocations={noLocations} setNoLocations={setNoLocations}/>
        {
          multipleCities.length > 0 ?
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box className="modalBox">
              <Typography variant='h4' className="modalTitle">Multiple Cities Detected</Typography>
              <Typography className="modalSubTitle">Please choose desired city</Typography>
              {
                multipleCities.map(city =>{ 
                  return<Grid className="modalGrid" onClick={() => handleSelectCity(city)}>
                    <Typography className="modalSelectionTitle">{city.name}, {city.state} {city.country}</Typography>
                  </Grid>
                })
              }
            </Box>
          </Modal> : null
        }
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="7day" element={<SevenDay />} />
          <Route path="profile" element={<Profile />} />
          <Route path="edit-locations" element={<EditLocations />} />
        </Routes>
      </AppInfoContext.Provider>
    </div>
  );
}

export default App;

import React from "react";
import { CityInfoModel } from "../routes/Home";
export interface IAppInfo {
  tempScale: string,
  setTempScale: (scale:string) => void,
  city: string,
  setCity: (city:string) => void,
  timeLength: string,
  setTimeLength: (length:string) => void,
  currentCity: CityInfoModel,
  setCurrentCity: (newCity:CityInfoModel) => void,
  weatherInfo: any,
  setWeatherInfo: (newInfo:any) => void,
  multipleCities: CityInfoModel[],
  setMultipleCities: (cities:CityInfoModel[]) => void,
  multipleCitiesModal: boolean,
  setMultipleCitiesModal: (modalState:boolean) => void,
  handleTempScaleChange: (newScale:string) => void,
  handleCityChange: (text:any) => void,
  handleCitySubmit: () => void,
  loggedIn: boolean, 
  setLoggedIn: (accountStatus:boolean) => void,
  savedLocations: Array<any>,
  setSavedLocation: (locations:Array<any>) => void
}
export const AppInfoContext = React.createContext<IAppInfo>({
  tempScale: "metric", 
  setTempScale: () => {return},
  city: "",
  setCity: () => {return},
  timeLength: "hourly",
  setTimeLength: () => {return},
  currentCity: {
    country: "CA", 
    state: "Ontario", 
    name: "Toronto",
    lat: 43.6534817,
    long: -79.3839347
  },
  setCurrentCity: () => {return},
  weatherInfo: {},
  setWeatherInfo: () => {return},
  multipleCities: [],
  setMultipleCities: () => {return},
  multipleCitiesModal: false,
  setMultipleCitiesModal: () => {return},
  handleTempScaleChange: () => {return},
  handleCityChange: () => {return},
  handleCitySubmit: () => {return},
  loggedIn: false,
  setLoggedIn: () => {return},
  savedLocations: [],
  setSavedLocation: () => {return} 
});